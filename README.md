Terminal zalgo text generator written in Python.

`pylgo.py` optinally supports copying output to the clipboard using
the Python library `Pyperclip`.
```
usage: pylgo [-h] [-t TOP] [-m MIDDLE] [-b BOTTOM] [-a ALL] [-u] [-f] [-c]
             input

positional arguments:
  input                 Text or file to (un)zalgofy

optional arguments:
  -h, --help            show this help message and exit
  -t TOP, --top TOP     Number of accents in the top row
  -m MIDDLE, --middle MIDDLE
                        Number of accents in the middle row
  -b BOTTOM, --bottom BOTTOM
                        Number of accents in the bottom row
  -a ALL, --all ALL     Number of accents in all rows
  -u, --unzalgo         Unzalgofy text
  -f, --file            Read from file instead of cli argument
  -c, --copy            Save output to the clipboard
```
