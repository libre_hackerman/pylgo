#!/usr/bin/env python3

# Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from random import shuffle
from argparse import ArgumentParser
from sys import stderr
try:
    from pyperclip import copy
    copy_available = True
except ImportError:
    copy_available = False


def read_file(path):
    try:
        with open(path) as f:
            lines = f.readlines()
        return "".join(lines)
    except Exception as e:
        print(str(e), file=stderr)
        exit(2)


class WrongNumber(Exception):
    pass


class Zalgo:
    def __init__(self, top_row, middle_row, bottom_row):
        self.TOP_ACCENTS = ["\u030d", "\u030e", "\u0304", "\u0305",
                            "\u033f", "\u0311", "\u0306", "\u0310",
                            "\u0352", "\u0357", "\u0351", "\u0307",
                            "\u0308", "\u030a", "\u0342", "\u0343",
                            "\u0344", "\u034a", "\u034b", "\u034c",
                            "\u0303", "\u0302", "\u030c", "\u0350",
                            "\u0300", "\u0301", "\u030b", "\u030f",
                            "\u0312", "\u0313", "\u0314", "\u033d",
                            "\u0309", "\u0363", "\u0364", "\u0365",
                            "\u0366", "\u0367", "\u0368", "\u0369",
                            "\u036a", "\u036b", "\u036c", "\u036d",
                            "\u036e", "\u036f", "\u033e", "\u035b",
                            "\u0346", "\u031a"]
        self.MIDDLE_ACCENTS = ["\u0315", "\u031b", "\u0340", "\u0341",
                               "\u0358", "\u0321", "\u0322", "\u0327",
                               "\u0328", "\u0334", "\u0335", "\u0336",
                               "\u034f", "\u035c", "\u035d", "\u035e",
                               "\u035f", "\u0360", "\u0362", "\u0338",
                               "\u0337", "\u0361", "\u0489"]
        self.BOTTOM_ACCENTS = ["\u0316", "\u0317", "\u0318", "\u0319",
                               "\u031c", "\u031d", "\u031e", "\u031f",
                               "\u0320", "\u0324", "\u0325", "\u0326",
                               "\u0329", "\u032a", "\u032b", "\u032c",
                               "\u032d", "\u032e", "\u032f", "\u0330",
                               "\u0331", "\u0332", "\u0333", "\u0339",
                               "\u033a", "\u033b", "\u033c", "\u0345",
                               "\u0347", "\u0348", "\u0349", "\u034d",
                               "\u034e", "\u0353", "\u0354", "\u0355",
                               "\u0356", "\u0359", "\u035a", "\u0323"]

        self.top_row = top_row
        self.middle_row = middle_row
        self.bottom_row = bottom_row
        self._check_n_accents()


    def _check_n_accents(self):
        if self.top_row > len(self.TOP_ACCENTS) or self.top_row < 0:
            raise WrongNumber(f"Top accents allowed range is [0,{len(self.TOP_ACCENTS)}]")
        elif self.middle_row > len(self.MIDDLE_ACCENTS) or self.middle_row < 0:
            raise WrongNumber(f"Middle accents allowed range is [0,{len(self.MIDDLE_ACCENTS)}]")
        elif self.bottom_row > len(self.BOTTOM_ACCENTS) or self.bottom_row < 0:
            raise WrongNumber(f"Bottom accents allowed range is [0,{len(self.BOTTOM_ACCENTS)}]")

    def _add_accents(self, letter):
        if self.top_row > 0:
            shuffle(self.TOP_ACCENTS)
            for i in range(self.top_row):
                letter += self.TOP_ACCENTS[i]

        if self.middle_row > 0:
            shuffle(self.MIDDLE_ACCENTS)
            for i in range(self.middle_row):
                letter += self.MIDDLE_ACCENTS[i]

        if self.bottom_row > 0:
            shuffle(self.BOTTOM_ACCENTS)
            for i in range(self.bottom_row):
                letter += self.BOTTOM_ACCENTS[i]

        return letter

    def zalgofy(self, source, is_file):
        zalgofied_text = ""
        text = read_file(source) if is_file else source

        for c in text:
            zalgofied_text += self._add_accents(c)

        return zalgofied_text

    def unzalgofy(source, is_file):
        zalgo = read_file(source) if is_file else source
        # Allow ascii characters and other non-ascii letters (normal accents)
        return "".join(filter(lambda c: c.isascii() or c.isalpha(), zalgo))


class Pylgo:
    def __init__(self):
        self.parser = ArgumentParser()
        self.parser.add_argument("input", help="Text or file to (un)zalgofy")
        self.parser.add_argument("-t", "--top", type=int, default=2,
                                 help="Number of accents in the top row")
        self.parser.add_argument("-m", "--middle", type=int, default=2,
                                 help="Number of accents in the middle row")
        self.parser.add_argument("-b", "--bottom", type=int, default=2,
                                 help="Number of accents in the bottom row")
        self.parser.add_argument("-a", "--all", type=int,
                                 help="Number of accents in all rows")
        self.parser.add_argument("-u", "--unzalgo", action="store_true",
                                 help="Unzalgofy text")
        self.parser.add_argument("-f", "--file", action="store_true",
                                 help="Read from file instead of cli argument")
        if copy_available:
            self.parser.add_argument("-c", "--copy", action="store_true",
                                     help="Save output to the clipboard")

    def start(self):
        args = self.parser.parse_args()

        if args.unzalgo:
            print(Zalgo.unzalgofy(args.input, args.file))
        else:
            try:
                if args.all != None:  # Same number for all rows
                    z = Zalgo(args.all, args.all, args.all)
                else:  # Custom number for each row
                    z = Zalgo(args.top, args.middle, args.bottom)

                zalgo_text = z.zalgofy(args.input, args.file)

                if copy_available and args.copy:
                    copy(zalgo_text)
                else:
                    print(zalgo_text)
            except WrongNumber as e:
                print(str(e), file=stderr)
                exit(1)


if __name__ == "__main__":
    Pylgo().start()
